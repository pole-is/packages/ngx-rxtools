import { OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, convertToParamMap, ParamMap } from '@angular/router';
import { Observable, ReplaySubject } from 'rxjs';
import { marbles } from 'rxjs-marbles';

import { QueryParam, RouteParam } from './route-param.decorator';

function testOne(decorator: any, mapProperty: string) {
  function mockActivatedRoute(
    map$: Observable<ParamMap>,
    snapshot: any
  ): ActivatedRoute {
    // tslint:disable-next-line:rxjs-finnish
    return {
      [mapProperty]: map$,
      snapshot: { [mapProperty]: snapshot },
    } as any;
  }

  class FakeComponent implements OnInit, OnDestroy {
    @decorator() private readonly param$ = new ReplaySubject<string>(1);

    // tslint:disable-next-line:no-unused-variable
    public constructor(private readonly route: ActivatedRoute) {}

    public getParam$(): Observable<string> {
      return this.param$;
    }

    public ngOnInit(): void {}

    public ngOnDestroy(): void {}
  }

  it(
    'should subscribe to paramMap on init',
    marbles(m => {
      const map$ = m.cold('');
      const comp = new FakeComponent(
        mockActivatedRoute(map$, convertToParamMap({}))
      );

      comp.ngOnInit();

      m.expect(map$).toHaveSubscriptions('^');

      m.flush();
      expect(comp).toBeTruthy();
    })
  );

  it(
    'should unsubscribe from paramMap on destroy',
    marbles(m => {
      const map$ = m.cold('');
      const comp = new FakeComponent(
        mockActivatedRoute(map$, convertToParamMap({}))
      );

      comp.ngOnInit();
      comp.ngOnDestroy();

      m.expect(map$).toHaveSubscriptions('(^!)');

      m.flush();
      expect(comp).toBeTruthy();
    })
  );

  it(
    'should propagate values to the observable property',
    marbles(m => {
      const VALUES = {
        a: convertToParamMap({ param: 'foo' }),
        b: convertToParamMap({ param: 'bar' }),
        c: convertToParamMap({ param: 'quz' }),
      };
      const map$ = m.hot('--a--b-^-c--|', VALUES);
      const comp = new FakeComponent(mockActivatedRoute(map$, VALUES.b));

      m.expect(comp.getParam$()).toBeObservable('b-c--|', {
        b: 'bar',
        c: 'quz',
      });

      comp.ngOnInit();

      m.flush();
      expect(comp).toBeTruthy();
    })
  );
}

describe('RouteParam', () => testOne(RouteParam, 'paramMap'));
describe('QueryParam', () => testOne(QueryParam, 'queryParamMap'));
