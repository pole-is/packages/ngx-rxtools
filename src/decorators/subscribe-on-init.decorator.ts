import { OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { hookBefore, hookFinally } from '../hooks';

const SUBSCRIPTION_KEY = Symbol.for('SubscribeOnInit');

interface AutoSubscribable extends OnInit, OnDestroy {}

type ObjectKey = string | number | symbol;

type AutoSubscriber<Name extends ObjectKey> = AutoSubscribable & {
  [SUBSCRIPTION_KEY]: Subscription;
} & {
    [K in Name]?: Observable<any>;
  };

/**
 * Ce décortaeur est à utiliser sur des propriétés de type Observable dans un composant implémentant OnInit
 * et OnDestroy. Il s'assure qu'un abonnnement à l'observavble est fait dans ngOnInit et que le désabonnement
 * est géré dans ngOnDestroy().
 */
export function SubscribeOnInit() {
  return <Target extends AutoSubscribable, Name extends ObjectKey>(
    target: Target,
    name: Name
  ): any => {
    const prototype = (target as any) as AutoSubscriber<Name>;

    hookBefore(prototype, 'ngOnInit', function() {
      if (!(this as any)[SUBSCRIPTION_KEY]) {
        (this as any)[SUBSCRIPTION_KEY] = new Subscription(
          () => delete (this as any)[SUBSCRIPTION_KEY]
        );
      }
      if ((this as any)[name]) {
        (this as any)[SUBSCRIPTION_KEY].add((this as any)[name].subscribe());
      }
    });

    hookFinally(prototype, 'ngOnDestroy', function() {
      if (this[SUBSCRIPTION_KEY]) {
        this[SUBSCRIPTION_KEY].unsubscribe();
      }
    });
  };
}
