import { OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { marbles } from 'rxjs-marbles/jasmine';

import { SubscribeOnInit } from './subscribe-on-init.decorator';

describe('@SubscribeOnInit', () => {
  class PropertyTestClass<T extends Observable<any>>
    implements OnInit, OnDestroy {
    @SubscribeOnInit() public readonly obs$: T;

    public constructor(
      obs$: T,
      private readonly initSpy = (): void => undefined,
      private readonly destroySpy = (): void => undefined
    ) {
      this.obs$ = obs$;
    }

    public ngOnInit(): void {
      this.initSpy();
    }

    public ngOnDestroy(): void {
      this.destroySpy();
    }
  }

  it(
    'should subscribe on init and unsubscribe on destroy',
    marbles(m => {
      const obj = new PropertyTestClass(m.cold('a'));

      obj.ngOnInit();
      obj.ngOnDestroy();

      m.expect(obj.obs$).toHaveSubscriptions('(^!)');
    })
  );

  it(
    'should be tied only to object instances',
    marbles(m => {
      const obj1 = new PropertyTestClass(m.cold('a'));
      const obj2 = new PropertyTestClass(m.cold('a'));

      obj1.ngOnInit();
      obj2.ngOnInit();

      obj1.ngOnDestroy();
      obj2.ngOnDestroy();

      m.expect(obj1.obs$).toHaveSubscriptions('(^!)');
      m.expect(obj2.obs$).toHaveSubscriptions('(^!)');
    })
  );

  it(
    'should call original ngOnInit',
    marbles(m => {
      const obs$ = m.cold('a');
      const initSpy = jasmine.createSpy('ngOnInit');
      const obj = new PropertyTestClass(obs$, initSpy);

      obj.ngOnInit();

      expect(initSpy).toHaveBeenCalled();
    })
  );

  it(
    'should call original ngOnDestroy',
    marbles(m => {
      const obs$ = m.cold('a');
      const destroySpy = jasmine.createSpy('ngOnDestroy');
      const obj = new PropertyTestClass(obs$, undefined, destroySpy);

      obj.ngOnInit();
      obj.ngOnDestroy();

      expect(destroySpy).toHaveBeenCalled();
    })
  );

  it(
    'can be used on more than one property',
    marbles(m => {
      class TwoPropertyTestClass implements OnInit, OnDestroy {
        @SubscribeOnInit() public readonly obs1$ = m.cold('a');

        @SubscribeOnInit() public readonly obs2$ = m.cold('b');

        public ngOnDestroy(): void {}

        public ngOnInit(): void {}
      }

      const obj = new TwoPropertyTestClass();

      obj.ngOnInit();
      obj.ngOnDestroy();

      m.expect(obj.obs1$).toHaveSubscriptions('(^!)');
      m.expect(obj.obs2$).toHaveSubscriptions('(^!)');
    })
  );

  it(
    'should be inherited',
    marbles(m => {
      class ParentClass implements OnInit, OnDestroy {
        @SubscribeOnInit() public readonly obs1$ = m.cold('a');

        public ngOnDestroy(): void {}

        public ngOnInit(): void {}
      }

      class ChildrenClass extends ParentClass {
        @SubscribeOnInit() public readonly obs2$ = m.cold('a');
      }

      const obj = new ChildrenClass();

      obj.ngOnInit();
      obj.ngOnDestroy();

      m.expect(obj.obs1$).toHaveSubscriptions('(^!)');
      m.expect(obj.obs2$).toHaveSubscriptions('(^!)');
    })
  );
});
