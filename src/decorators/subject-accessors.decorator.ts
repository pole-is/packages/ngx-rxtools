/**
 * Génère un getter et un setter publics pour un attribut de type BehaviorSubject.
 *
 * Le setter utilise .next() et le getter .getValue().
 *
 * Le nom de la propriété doit se terminer par '$'.
 */
export function SubjectAccessors() {
  return (prototype: object, observablePropertyName: string | symbol): void => {
    Object.defineProperty(
      prototype,
      getPlainPropertyName(observablePropertyName),
      {
        get(): any {
          return this[observablePropertyName].getValue();
        },
        set(value: any): void {
          this[observablePropertyName].next(value);
        },
      }
    );
  };
}

/**
 * Génère un setter public pour un attribut de type Subject. Il utilise utilise .next().
 *
 * Le nom de la propriété doit se terminer par '$'.
 */
export function SubjectSetter() {
  return (prototype: object, observablePropertyName: string | symbol): void => {
    Object.defineProperty(
      prototype,
      getPlainPropertyName(observablePropertyName),
      {
        set(value: any): void {
          this[observablePropertyName].next(value);
        },
      }
    );
  };
}

/**
 * Extrait le nom de propriété "normale" d'un nom de propriété de "setter".
 */
function getPlainPropertyName(observablePropertyName: string | symbol): string {
  const name = observablePropertyName.toString();
  if (!name.endsWith('$')) {
    throw new Error(`Property name must end with '$', '${name}' does noit`);
  }
  return name.substr(0, name.length - 1);
}
