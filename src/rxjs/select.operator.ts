import { Observable, Observer, OperatorFunction } from 'rxjs';

type State<T, R, V> =
  | { readonly status: 'NEW' }
  | {
      readonly status: 'OPEN';
      readonly input: T;
      readonly output: R;
      readonly values: V;
    }
  | { readonly status: 'CLOSED' };

export function select<T, R, V extends any[]>(
  selectors: { [I in keyof V]: (input: T) => V[I] },
  projector: (...args: V) => R,
  isSame: <X>(a: X, b: X) => boolean = <Y>(a: Y, b: Y) => a === b
): OperatorFunction<T, R> {
  return (source$: Observable<T>) =>
    new Observable<R>(subscriber => {
      let state: State<T, R, V> = { status: 'NEW' };

      function close() {
        state = { status: 'CLOSED' };
      }

      const observer: Observer<T> = {
        get closed(): boolean {
          return state.status === 'CLOSED';
        },

        next: (input: T) => {
          if (state.status === 'CLOSED') {
            return;
          }

          if (state.status === 'OPEN' && isSame(input, state.input)) {
            subscriber.next(state.output);
            return;
          }

          const values = selectors.map(s => s(input)) as V;
          if (
            state.status === 'OPEN' &&
            state.values.every((v, i) => isSame(v, values[i]))
          ) {
            subscriber.next(state.output);
            return;
          }

          const output = projector(...values);

          state = { status: 'OPEN', input, values, output };
          subscriber.next(output);
        },

        error: err => {
          subscriber.error(err);
          close();
        },

        complete: () => {
          subscriber.complete();
          close();
        },
      };

      const sub = source$.subscribe(observer);
      sub.add(close);
      return sub;
    });
}
