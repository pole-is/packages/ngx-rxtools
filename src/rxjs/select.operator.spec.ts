import { marbles } from 'rxjs-marbles';

import { select } from './select.operator';

describe('select', () => {
  const VALUES = {
    a: 1,
    b: 2,
    A: 4,
    B: 13,
  };

  const sub5 = jasmine.createSpy('sub5');
  sub5.and.callFake((x: number) => x - 5);

  const mul8 = jasmine.createSpy('mul8');
  mul8.and.callFake((x: number) => x * 8);

  const const2 = jasmine.createSpy('const2');
  const2.and.callFake(() => 2);

  const add = jasmine.createSpy('add');
  add.and.callFake((x: number, y: number) => x + y);

  beforeEach(() => {
    sub5.calls.reset();
    mul8.calls.reset();
    add.calls.reset();
  });

  it('should produce the expected value', () => {
    marbles(m => {
      const source$ = m.cold('--a--|', VALUES);
      const expected = '--A--|';
      const actual$ = source$.pipe(select([sub5, mul8], add));

      m.expect(actual$).toBeObservable(expected, VALUES);
    })();

    expect(sub5).toHaveBeenCalledTimes(1);
    expect(mul8).toHaveBeenCalledTimes(1);
    expect(add).toHaveBeenCalledTimes(1);

    expect(sub5).toHaveBeenCalledWith(VALUES.a);
    expect(mul8).toHaveBeenCalledWith(VALUES.a);
    expect(add).toHaveBeenCalledWith(VALUES.a - 5, VALUES.a * 8);
  });

  it('should recalculate on different input', () => {
    marbles(m => {
      const source$ = m.cold('--a--b--|', VALUES);
      const expected = '--A--B--|';
      const actual$ = source$.pipe(select([sub5, mul8], add));

      m.expect(actual$).toBeObservable(expected, VALUES);
    })();

    expect(sub5).toHaveBeenCalledWith(VALUES.a);
    expect(mul8).toHaveBeenCalledWith(VALUES.a);
    expect(add).toHaveBeenCalledWith(VALUES.a - 5, VALUES.a * 8);

    expect(sub5).toHaveBeenCalledWith(VALUES.b);
    expect(mul8).toHaveBeenCalledWith(VALUES.b);
    expect(add).toHaveBeenCalledWith(VALUES.b - 5, VALUES.b * 8);
  });

  it('should not recalculate on same input', () => {
    marbles(m => {
      const source$ = m.cold('--a--a--|', VALUES);
      const expected = '--A--A--|';
      const actual$ = source$.pipe(select([sub5, mul8], add));

      m.expect(actual$).toBeObservable(expected, VALUES);
    })();

    expect(sub5).toHaveBeenCalledTimes(1);
    expect(mul8).toHaveBeenCalledTimes(1);
    expect(add).toHaveBeenCalledTimes(1);
  });

  it('should not recalculate on same intermediates values', () => {
    marbles(m => {
      const source$ = m.cold('--a--b--|', VALUES);
      const expected = '--A--A--|';
      const actual$ = source$.pipe(select([const2, const2], add));

      m.expect(actual$).toBeObservable(expected, VALUES);
    })();

    expect(add).toHaveBeenCalledWith(2, 2);
    expect(add).toHaveBeenCalledTimes(1);
  });
});
