import { combineLatest, Observable, ObservableInput } from 'rxjs';

/**
 * Observable qui envoie un tableau vide et ne complète pas.
 */
// tslint:disable-next-line:rxjs-finnish
const EMPTY_ARRAY_OBSERVABLE: Observable<any[]> = new Observable(subscriber =>
  subscriber.next([])
);

/**
 * Variante de combineLatest qui transmets un tableau vide si le tableau des entrées est vide.
 */
export function safeCombineLatest<T>(
  inputs$: Array<ObservableInput<T>>
): Observable<T[]> {
  if (inputs$.length === 0) {
    return EMPTY_ARRAY_OBSERVABLE;
  }

  return combineLatest(inputs$);
}
