import { OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { marbles } from 'rxjs-marbles';

import { untilDestroyed } from './until-destroyed.operator';

describe('untilDestroyed', () => {
  class TestClass implements OnInit, OnDestroy {
    public constructor(private readonly obs$: Observable<any>) {}

    public ngOnInit(): void {
      this.obs$.pipe(untilDestroyed(this)).subscribe();
    }

    public ngOnDestroy(): void {}
  }

  it(
    'should unsubscribe on destroy',
    marbles(m => {
      const o$ = m.cold('--a--|');
      const obj = new TestClass(o$);

      obj.ngOnInit();
      obj.ngOnDestroy();

      m.flush();

      m.expect(o$).toHaveSubscriptions('(^!)');
      expect(obj).not.toBeUndefined();
    })
  );
});
