import { Observer } from 'rxjs';

let serial = 0;

// tslint:disable:no-console
export function consoleObserver<T>(subject = `spy#${++serial}`): Observer<T> {
  return <Observer<T>>{
    closed: false,
    next: value => console.log(subject, 'next:', value),
    error: err => console.log(subject, 'error:', err),
    complete: () => console.log(subject, 'completed.'),
  };
}
