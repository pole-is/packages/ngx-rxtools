import { forkJoin, from, Observable, ObservableInput, of } from 'rxjs';
import { take } from 'rxjs/operators';

/**
 * Variante de forkJoin qui :
 * - renvoie un tableau vide si le tableau d'entrée est vide.
 * - retourne uniquement la première valeure de chaque entrée.
 */
export function safeForkJoin<T>(
  inputs$: Array<ObservableInput<T>>
): Observable<T[]> {
  if (inputs$.length === 0) {
    return of([]);
  }

  return forkJoin(inputs$.map(input$ => from(input$).pipe(take(1))));
}
