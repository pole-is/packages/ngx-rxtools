import { Observable } from 'rxjs';
import { marbles } from 'rxjs-marbles/jasmine';

import { safeForkJoin } from './safe-fork-join.observable';

describe('safe-fork-join', () => {
  const INPUTS: { [key: string]: number } = {
    a: 0,
    b: 1,
    c: 2,
    d: 3,
    e: 4,
  };

  const OUTPUTS: { [key: string]: number[] } = {
    N: [],
    A: [0],
    V: [0, 3, 4],
  };

  it(
    'should emit an empty array and complete on empty inputs',
    marbles(m => {
      const inputs$: Array<Observable<number>> = [
        // EMPTY
      ];
      const output = '(N|)';

      m.expect(safeForkJoin(inputs$)).toBeObservable(output, OUTPUTS);
    })
  );

  it(
    'should emit an 1-sized array on 1-sized input',
    marbles(m => {
      const inputs$: Array<Observable<number>> = [
        //
        m.cold('a', INPUTS),
      ];
      const output = '(A|)';

      m.expect(safeForkJoin(inputs$)).toBeObservable(output, OUTPUTS);
    })
  );

  it(
    'should emit only the first value of each inputs',
    marbles(m => {
      const inputs$: Array<Observable<number>> = [
        //
        m.cold('a-b-c', INPUTS),
        m.cold('d', INPUTS),
        m.cold('--e', INPUTS),
      ];
      const output = '--(V|)';

      m.expect(safeForkJoin(inputs$)).toBeObservable(output, OUTPUTS);
    })
  );
});
