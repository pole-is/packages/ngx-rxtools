import { Observable } from 'rxjs';
import { marbles } from 'rxjs-marbles/jasmine';

import { safeCombineLatest } from './safe-combine-latest.observable';

describe('safe-combine-latest', () => {
  const INPUTS: { [key: string]: number } = {
    a: 0,
    b: 1,
    c: 2,
    d: 3,
    e: 4,
  };

  const OUTPUTS: { [key: string]: number[] } = {
    N: [],
    A: [0],
    B: [1],
    C: [2],
    U: [1, 3, 4],
    V: [2, 3, 4],
  };

  it(
    'should emit an empty array on empty inputs',
    marbles(m => {
      const inputs$: Array<Observable<number>> = [
        // EMPTY
      ];
      const output = 'N';

      m.expect(safeCombineLatest(inputs$)).toBeObservable(output, OUTPUTS);
    })
  );

  it(
    'should emit an 1-sized array on 1-sized input',
    marbles(m => {
      const inputs$: Array<Observable<number>> = [
        //
        m.cold('abc', INPUTS),
      ];
      const output = 'ABC';

      m.expect(safeCombineLatest(inputs$)).toBeObservable(output, OUTPUTS);
    })
  );

  it(
    'should emit combinations of each inputs',
    marbles(m => {
      const inputs$: Array<Observable<number>> = [
        //
        m.cold('a-b-c', INPUTS),
        m.cold('d', INPUTS),
        m.cold('--e', INPUTS),
      ];
      const output = '--U-V';

      m.expect(safeCombineLatest(inputs$)).toBeObservable(output, OUTPUTS);
    })
  );
});
