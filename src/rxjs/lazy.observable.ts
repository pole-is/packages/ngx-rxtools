import { defer, Observable, ObservedValueOf } from 'rxjs';

/**
 * Un observable qui attend le premier abonnement pour fabriquer l'observable qui va vraiment être
 * utilisé.
 */
export function lazy<O extends Observable<any>>(
  create: () => O
): Observable<ObservedValueOf<O>> {
  let obs: O | null = null;
  return defer(() => (obs !== null ? obs : (obs = create())));
}
