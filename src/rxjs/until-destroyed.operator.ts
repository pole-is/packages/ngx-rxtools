import { OnDestroy } from '@angular/core';
import { MonoTypeOperatorFunction, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { hookFinally } from '../hooks';

const OPERATOR = Symbol.for('untilDestroyedOperator');

/**
 * Opérateur qui interrompt l'observable quand la méthode "ngOnDestroy" de l'objet passé
 * en paramètre est appelée.
 *
 * @param {OnDestroy} target
 * @return {MonoTypeOperatorFunction<T>}
 *
 * @example
 *    class Foo extends OnInit, OnDestroy {
 *      private readonly data$: Observable<string>;
 *
 *      public ngOnInit() {
 *        this.data$ = from(['a', 'b', 'c']).pipe(untilDestroyed(this));
 *      }
 *
 *      public ngOnDestroy() {}
 *    }
 */
export function untilDestroyed<T>(
  target: OnDestroy
): MonoTypeOperatorFunction<T> {
  if (OPERATOR in target) {
    return (target as any)[OPERATOR];
  }

  const signal$ = new Subject<void>();
  const operator = takeUntil<T>(signal$);

  (target as any)[OPERATOR] = operator;
  hookFinally(target, 'ngOnDestroy', () => signal$.next(undefined));

  return operator;
}
